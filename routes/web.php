<?php
//Route::get('/', function () {
//    return view('welcome');
//});
//测试首页
Route::get('/', 'PagesController@root')->name('root');
//make:auth 自带用户认证
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//修改个人页面路由
Route::resource('users', 'UsersController', ['only' => ['show', 'edit', 'update']]);
Route::resource('topics', 'TopicsController', ['only' => ['index', 'create', 'store', 'update', 'edit', 'destroy']]);
//分类下的话题列表
Route::resource('categories', 'CategoriesController', ['only' => ['show']]);
//上传图片
Route::post('upload_image', 'TopicsController@uploadImage')->name('topics.upload_image');
//seo
Route::get('topics/{topic}/{slug?}', 'TopicsController@show')->name('topics.show');
Route::resource('replies', 'RepliesController', ['only' => [ 'store','destroy']]);
//显示通知
Route::resource('notifications', 'NotificationsController', ['only' => ['index']]);
//非法访问警告
Route::get('permission-denied', 'PagesController@permissionDenied')->name('permission-denied');