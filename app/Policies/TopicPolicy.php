<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Topic;

class TopicPolicy extends Policy
{
    //更新授权
    public function update(User $user, Topic $topic)
    {
        //当话题关联作者的 ID 等于当前登录用户 ID 时候才放行
        return $user->isAuthorOf($topic);
        //return true;
    }

    public function destroy(User $user, Topic $topic)
    {
        //return true;
        return $user->isAuthorOf($topic);
    }
}
