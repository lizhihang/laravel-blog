<?php

namespace App\Observers;

use App\Models\Reply;
use App\Notifications\TopicReplied;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class ReplyObserver
{
    //创建后
    public function created(Reply $reply)
    {
        $topic = $reply->topic;
        $topic->increment('reply_count', 1);
        //通知作者被回复了话题
        //默认的 User 模型中使用了 trait —— Notifiable，
        //它包含着一个可以用来发通知的方法 notify() ，
        //此方法接收一个通知实例做参数。
        $topic->user->notify(new TopicReplied($reply));
    }

    //xss 创建的时候进行xss过滤
    public function creating(Reply $reply)
    {
        $reply->content = clean($reply->content, 'user_topic_body');
    }

    //删除回复后 减少回复数量
    public function deleted(Reply $reply)
    {
        if ($reply->topic->reply_count > 0) {
            $reply->topic->decrement('reply_count', 1);
        }
    }
}