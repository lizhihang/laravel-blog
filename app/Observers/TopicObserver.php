<?php

namespace App\Observers;

use App\Handlers\SlugTranslateHandler;
use App\Jobs\TranslateSlug;
use App\Models\Topic;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

class TopicObserver
{
    public function saving(Topic $topic)
    {
        //xss过滤
        $topic->body = clean($topic->body, 'user_topic_body');
        // Topic 模型保存时触发的 saving 事件中，对 excerpt 字段进行赋值：
        $topic->excerpt = make_excerpt($topic->body);
//        //seo 翻译 title
//        if ( !$topic->slug) {
//            //推送任务队列
//            dispatch(new TranslateSlug($topic));
//            //$topic->slug = app(SlugTranslateHandler::class)->translate($topic->title);
//        }
    }

    //模型监控器的 saved() 方法对应 Eloquent 的 saved 事件，
    //此事件发生在创建和编辑时、数据入库以后。
    //在 saved() 方法中调用，确保了我们在分发任务时，$topic->id 永远有值。
    public function saved(Topic $topic)
    {
        if ( !$topic->slug) {
            //推送任务队列
            dispatch(new TranslateSlug($topic));
            //$topic->slug = app(SlugTranslateHandler::class)->translate($topic->title);
        }
    }

    //删除话题就删除话题下的所有回复
    public function deleted(Topic $topic)
    {
        \DB::table('replies')->where('topic_id', $topic->id)->delete();
    }
}