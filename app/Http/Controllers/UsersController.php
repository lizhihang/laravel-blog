<?php

namespace App\Http\Controllers;

use App\Handlers\ImageUploadHandler;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;

//个人页面
class UsersController extends Controller
{
    public function __construct()
    {
        //未登录只有查看的权限
        $this->middleware('auth', ['except' => ['show']]);
    }

    //
    public function show(User $user)
    {
        //用户对象变量 $user 通过 compact 方法转化为一个关联数组，
        //并作为第二个参数传递给 view 方法，将变量数据传递到视图中。
        return view('users.show', compact('user'));
    }

    //编辑
    public function edit(User $user)
    {
        $this->authorize('update',$user);
        return view('users.edit', compact('user'));
    }

    //更新
    public function update(UserRequest $request, User $user, ImageUploadHandler $uploader)
    {
        //dd($request->avatar);
        $this->authorize('update',$user);
        $data = $request->all();
        if ($request->avatar) {
            $result = $uploader->save($request->avatar, 'avatars', $user->id, 362);
            if ($result) {
                $data['avatar'] = $result['path'];
            }
        }
        $user->update($data);
        return redirect()->route('users.show', $user->id)->with('success', '更新个人资料成功');
    }
}
