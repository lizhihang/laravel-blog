<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //可以被批量赋值的字段
    protected $fillable = [
        'name', 'description',
    ];
}
