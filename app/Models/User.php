<?php

namespace App\Models;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    //需在 User 中使用 laravel-permission 提供的 Trait —— HasRoles，
    //此举能让我们获取到扩展包提供的所有权限和角色的操作方法。
    use HasRoles;
    //use Notifiable;
    use Notifiable {
        notify as protected laravelNotify;
    }

    public function notify($instance)
    {
        //通知用户为作者不通知
        if ($this->id == Auth::id()) {
            return;
        }
        //每当你调用 $user->notify() 时， users 表里的 notification_count 将自动 +1。
        $this->increment('notification_count');
        //发送给定的通知。
        $this->laravelNotify($instance);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'introduction', 'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    //一用户可以拥有多条话题
    //关联设置成功后，我们即可使用 $user->topics 来获取到用户发布的所有话题数据
    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    //是否文章作者
    public function isAuthorOf($model)
    {
        return $this->id == $model->user_id;
    }

    //一个用户可以发布很多评论
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    //清除已读的消息
    public function markAsRead()
    {
        $this->notification_count = 0;
        $this->save();
        $this->unreadNotifications->markAsRead();
    }

    //后台修改密码
    public function setPasswordAttribute($value)
    {
        // 如果值的长度等于 60，即认为是已经做过加密的情况
        if (strlen($value) != 60) {

            // 不等于 60，做密码加密处理
            $value = bcrypt($value);
        }

        $this->attributes['password'] = $value;
    }
    //后台修改头像
    public function setAvatarAttribute($path)
    {
        // 如果不是 `http` 子串开头，那就是从后台上传的，需要补全 URL
        //确定给定字符串是否以给定的子字符串开头。
        if ( ! starts_with($path, 'http')) {

            // 拼接完整的 URL
            $path = config('app.url') . "/uploads/images/avatars/$path";
        }

        $this->attributes['avatar'] = $path;
    }
}
