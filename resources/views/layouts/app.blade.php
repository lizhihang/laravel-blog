<!doctype html>
{{--获取默认的语言=>app.php中的locale--}}
<html lang="{{app()->getLocale()}}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>@yield('title','laravel-blog') -{{setting('site_name','Laravel-bbs')}}</title>
    <meta name="description" content="@yield('description',setting('seo_description'), 'LaraBBS')" />
    <meta name="keyword" content="@yield('keyword', setting('seo_keyword', 'LaraBBS,社区,论坛,开发者论坛'))" />
    {{--style--}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    @yield('styles')
    {{--公共模版--}}
</head>
<body>
<div id="app" class="{{route_class()}}-page">
    @include('layouts._header')
    <div class="container">
        {{--消息提示--}}
        @include('layouts._message')
        @yield('content')
    </div>
    @include('layouts._footer')
</div>
{{--sudo-su插件--}}
@if (app()->isLocal())
    @include('sudosu::user-selector')
@endif
{{--script--}}
<script src="{{asset('js/app.js')}}"></script>
@yield('scripts')
</body>
</html>