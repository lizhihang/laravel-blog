<footer class="footer">
    {{--底部模版--}}
    <div class="container">
        {{--左边--}}
        <p class="pull-left">
            By <a href="" target="_blank">李志行</a><span style="color: #e27575;font-size: 14px;">❤</span>
        </p>
        {{--右边--}}
        <p class="pull-right"><a href="mailto:{{setting('contact_email')}}">联系我</a></p>
    </div>
</footer>
