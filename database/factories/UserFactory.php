<?php

use Faker\Generator as Faker;

use Carbon\Carbon;

$factory->define(App\Models\User::class, function (Faker $faker) {
    static $password;
    $now = Carbon::now()->toDateTimeString();
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        //不存在初始化密码为password
        'password' => $password ?: $password = bcrypt('password'), // password
        'remember_token' => str_random(10),
        //随机生成『小段落』文本
        'introduction' => $faker->sentence(),
        'created_at' => $now,
        'updated_at' => $now,
    ];
});
